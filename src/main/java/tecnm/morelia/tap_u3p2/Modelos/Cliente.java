/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tecnm.morelia.tap_u3p2.Modelos;

/**
 *
 * @author Rayfi
 */
public class Cliente {
    private int idCliente; //PK
    private int idUsuario; //FK -> 1:1 Usuario
    private String nombre;
    private String apellidos;
    private String calle;
    private String numExterior;
    private String numInterior;
    private String colonia;
    private String codigoPostal;
    private String ciudad;
    private String estado;
    private String correo;
    private String celular;
    private String mecanismosPago;
    private byte[] foto;

    
    
    //CREATE TABLE clientes(id_cliente int primary key auto_increment, id_usuario int not null,nombre varchar(150) not null, apellidos varchar(250) not null, calle varchar(250) not null,num_exterior varchar(50) not null, num_interior varchar(50), colonia varchar(250) not null,codigo_postal varchar(5) not null, ciudad varchar(250) not null, estado varchar(2) not null, correo varchar(250) not null, celular varchar(10), mecanismos_pago text, foto blob, foreign key (id_usuario) references usuario(id_usuario), foreign key (estado) references estados(id_estado) );

    public Cliente() {
    }

    public Cliente(int idCliente, int idUsuario, String nombre, String apellidos, String calle, String numExterior, String numInterior, String colonia, String codigoPostal, String ciudad, String estado, String correo, String celular, String mecanismosPago, byte[] foto) {
        this.idCliente = idCliente;
        this.idUsuario = idUsuario;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.calle = calle;
        this.numExterior = numExterior;
        this.numInterior = numInterior;
        this.colonia = colonia;
        this.codigoPostal = codigoPostal;
        this.ciudad = ciudad;
        this.estado = estado;
        this.correo = correo;
        this.celular = celular;
        this.mecanismosPago = mecanismosPago;
        this.foto = foto;
    }

    public Cliente(int idUsuario, String nombre, String apellidos, String calle, String numExterior, String colonia, String codigoPostal, String ciudad, String estado, String correo) {
        this.idUsuario = idUsuario;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.calle = calle;
        this.numExterior = numExterior;
        this.colonia = colonia;
        this.codigoPostal = codigoPostal;
        this.ciudad = ciudad;
        this.estado = estado;
        this.correo = correo;
    }

    public Cliente(String nombre, String apellidos, String calle, String numExterior, String colonia, String codigoPostal, String ciudad, String estado, String correo) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.calle = calle;
        this.numExterior = numExterior;
        this.colonia = colonia;
        this.codigoPostal = codigoPostal;
        this.ciudad = ciudad;
        this.estado = estado;
        this.correo = correo;
    }
    
    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumExterior() {
        return numExterior;
    }

    public void setNumExterior(String numExterior) {
        this.numExterior = numExterior;
    }

    public String getNumInterior() {
        return numInterior;
    }

    public void setNumInterior(String numInterior) {
        this.numInterior = numInterior;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getMecanismosPago() {
        return mecanismosPago;
    }

    public void setMecanismosPago(String mecanismosPago) {
        this.mecanismosPago = mecanismosPago;
    }

    
    
    @Override
    public String toString() {
        return "Cliente{" + "idCliente=" + idCliente + ", idUsuario=" + idUsuario + ", nombre=" + nombre + ", apellidos=" + apellidos + ", calle=" + calle + ", numExterior=" + numExterior + ", numInterior=" + numInterior + ", colonia=" + colonia + ", codigoPostal=" + codigoPostal + ", ciudad=" + ciudad + ", estado=" + estado + ", correo=" + correo + ", celular=" + celular + ", mecanismosPago=" + mecanismosPago + ", foto=" + foto + '}';
    }

    
    
    
    
}
