/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tecnm.morelia.tap_u3p2.Modelos;

import java.awt.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Rayfi
 */
public class ManejadorBD {
    private Connection conexion;
    private Statement sentencia;
    
    public ManejadorBD() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            
            conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/tienda_online","root","");
            
            sentencia = conexion.createStatement();
            
        
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void cerrarConexion() throws Exception{
        if(sentencia != null){
            sentencia.close();
        }
        if(conexion != null){
            conexion.close();
        }
    }
    
    public Usuario encuentraUsuario(String usuario, String pass) throws Exception{
        String query = "SELECT MD5('"+pass+"')";
        System.out.println(query);
        
        ResultSet rs = sentencia.executeQuery(query);
        rs.next();
        String encriptedPass = rs.getString(1);
        
        query = "SELECT * FROM usuario WHERE nombre_usuario='"+usuario+"' AND contrasenia='"+encriptedPass+"'";
        System.out.println(query);
        
        rs = sentencia.executeQuery(query);
        
        Usuario user = null;
        
        if(rs.next()){
            user = new Usuario();
            user.setIdUsuario(rs.getInt("id_usuario"));
            user.setNombreUsuario(rs.getString("nombre_usuario"));
            user.setContrasenia(encriptedPass);
            user.setTipo(rs.getString("tipo"));
        }
        rs.close();
        
        return user;
    } 
    
    
    public ArrayList<String> getDatosFROM_TABLE(String nombreTable, String _id, ArrayList<String> columns) throws Exception{
        String query = "SELECT * FROM "+nombreTable;
        String[] desc = new String[columns.size()];
        
        ResultSet rs = sentencia.executeQuery(query);
        ArrayList<String> resultados = new ArrayList<>();
        
        for(int it = 0; rs.next(); it++){
            int id = rs.getInt(_id);
            resultados.add("ID: "+id+",");
            
            for (int i = 0; i < columns.size(); i++) {
                desc[i] = rs.getString(columns.get(i));
                //System.out.println("::::: "+desc[i]);
                
                resultados.add(it,columns.get(i)+": "+desc[i]);
            }
        }
        rs.close();
        
        
        
        return resultados;
    }
    
    public ArrayList<String> getEstados() throws Exception{
        String query = "SELECT nombre_estado FROM estados";
        ResultSet rs = sentencia.executeQuery(query);
        ArrayList<String> resultados = new ArrayList<>();
        
        while(rs.next()){
            String desc = rs.getString("nombre_estado");
            
            resultados.add(desc);
        }
        
        return resultados;
    }
    
    public Cliente nuevoCliente(Cliente cliente, Usuario usuario) throws Exception{
        Cliente clienteNuevo = null;
        
        //Inicio de la transaccion
        conexion.setAutoCommit(false);
        
        
        String query = "INSERT INTO usuario VALUES (NULL,?,MD5(?),?)";
        PreparedStatement ps = conexion.prepareStatement(query);
        ps.setString(1, usuario.getNombreUsuario());
        ps.setString(2, usuario.getContrasenia());
        ps.setString(3, usuario.getTipo());

        int resultado = ps.executeUpdate();
        
        if(resultado > 0){
            query = "SELECT MAX(id_usuario) FROM usuario"; //MAX() da el ultimo valor ingresado
            
            int idUsuario = -1;
            
            ResultSet rs = sentencia.executeQuery(query);
            if(rs.next()){
                idUsuario = rs.getInt(1);
            }
            //rs.close();
            
            query = "INSERT INTO clientes VALUES(NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            
            PreparedStatement psC = conexion.prepareStatement(query);
            psC.setInt(1, idUsuario);
            psC.setString(2, cliente.getNombre());
            psC.setString(3,cliente.getApellidos());
            psC.setString(4,cliente.getCalle());
            psC.setString(5,cliente.getNumExterior());
            psC.setString(6, cliente.getNumInterior());
            psC.setString(7,cliente.getColonia());
            psC.setString(8,cliente.getCodigoPostal());
            psC.setString(9,cliente.getCiudad());
            psC.setString(10,cliente.getEstado());
            psC.setString(11, cliente.getCorreo());
            psC.setString(12,cliente.getCelular());
            psC.setString(13,cliente.getMecanismosPago());
            psC.setBytes(14, cliente.getFoto());
            
            int resultadoC = psC.executeUpdate();
            
            if(resultadoC > 0){
                query = "SELECT MAX(id_cliente) FROM clientes"; //MAX() da el ultimo valor ingresado
            
                int idCliente = -1;
            
                ResultSet rsC = sentencia.executeQuery(query);
                if(rsC.next()){
                    idCliente = rsC.getInt(1);
                    
                    cliente.setIdCliente(idCliente);
                    cliente.setIdUsuario(idUsuario);
                    
                    conexion.commit();
                    
                    clienteNuevo = cliente;
                }
                //rsC.close();
            }
            else{
                conexion.rollback();
            }
        }
        else{
            //Se hacer Rollback: Deshacer las querys
            conexion.rollback();
        }
        
        
        
        return clienteNuevo;
    }

}
